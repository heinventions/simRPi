from setuptools import setup, find_packages

from simRPi import __author__, __version__, __license__

setup(
    name='simRPi',
    version=__version__,
    description='simRPi.GPIO emulator',
    license=__license__,
    author=__author__,
    author_email='support@heinventions.com',
    url='https://gitlab.com/heinventions/simRPi',
    keywords='raspberry pi gpio emulator python3',
    packages=find_packages(),
    install_requires=[],
)
